<?php

namespace Drupal\changelog\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Changelog entities.
 */
interface ChangelogEntityInterface extends ConfigEntityInterface {

}
